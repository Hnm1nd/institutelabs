package ru;

import ru.inf_security.task4.Task4;

public class Main {

    public static void main(String[] args) {
//        log1("Task 1 started.");
//        new Task1().start();
//        log1("Task 1 finished. Task 2 started.");
//        new Task2().start();
//        log1("Task 2 finished.");
//        new Task3().start();
        new Task4().start();
    }

    private static void log1(String msg) {
        if (msg != null) {
            System.out.println(msg);
        }
    }
}
