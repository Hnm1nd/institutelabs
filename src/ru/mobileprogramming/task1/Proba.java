package ru.mobileprogramming.task1;

import java.io.IOException;

public class Proba {

    static int b;
    static char[] charArray;

    public static void main(String[] args) {
        charArray = new char[20];
        int i = 0;
        log("Enter the name: ");

        try {
            while((b = System.in.read()) != 10)  {
                charArray[i++] = (char) b;
            }
        } catch (IOException e) {
            log("Error: " + e.getLocalizedMessage());
        } catch (Throwable t) {
            log("Throwable: " + t.getMessage());
        }
        log("Hello, " + new String(charArray) + "!");
    }

    public static void log(String msg) {
        System.out.println(msg);
    }
}
