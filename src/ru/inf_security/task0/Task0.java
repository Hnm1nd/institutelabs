package ru.inf_security.task0;

import java.util.Random;

public class Task0 {

    /*
    14 variant
    6 = password length
        1.   - случайные заглавные буквы английского алфавита.
        2.  (где mod 10 – остаток от деления числа на 10).
        3.   - случайная цифра.
        4.   - случайный символ из множества {!,”,#,$,%,&,’,(,),*}.
        5.    - случайная малая буква английского алфавита.
     */

    private static Random random = new Random();

    private static int passwordLength = 6;

    public static String generatePassword() {
        String result = "";

        result += generateLetter(65, 90);
        result += generateLetter(65, 90);

        result += String.valueOf((passwordLength * passwordLength) % 10);

        result += generateLetter(48, 57);

        result += generateLetter(33, 42);

        result += generateLetter(97, 122);

        return result;
    }

    public static char generateLetter(int min, int max) {
        return (char) (random.nextInt(max - min) + min);
    }

}
