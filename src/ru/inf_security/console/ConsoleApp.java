package ru.inf_security.console;

import java.io.*;
import java.util.Scanner;

public class ConsoleApp {

    private boolean exit = false;
    private String username = null;

    private Scanner scanner;

    protected static final void log0(String msg) {
        System.out.print(msg);
    }

    protected static final void log1(String msg) {
        System.out.println(msg);
    }

    public final void start() {
        onStart();
    }

    protected void onStart() {
        init();
        while (!exit) {
            onIteration();
        }
        log1("Goodbye!");
    }

    protected void onIteration() {
        if (username == null) {
            requestAuthorization();
        } else {
            waitForCommand();
        }
    }

    protected void onInit() {
    }

    protected void onCommand(String command) {
    }

    protected void onAuthorization(boolean isSuccessful) {
    }

    protected boolean isUserValid(String username) {
        return false;
    }

    protected final Scanner getScanner() {
        return scanner;
    }

    protected final String getUsername() {
        return username;
    }

    private void init() {
        scanner = new Scanner(System.in);
        onInit();
    }

    protected void waitForCommand() {
        try {
            log0("> ");
            String command = scanner.nextLine();
            switch (command) {
                case "deauth": {
                    username = null;
                    break;
                }
                case "exit": {
                    exit = true;
                    break;
                }
                default: {
//                    if (getScanner().hasNextLine()) {
//                        getScanner().nextLine();
//                    }
                    onCommand(command);
                    break;
                }
            }
        } catch (Throwable t) {
            log1("Command execution failed. \n " + t.getMessage());
        }
    }

    protected void requestAuthorization() {
        log0("Enter the username: ");
        String username = scanner.next();
        if (username.equals("exit")) {
            exit = true;
            return;
        }
        if (isUserValid(username)) {
            log1("Authorization finished. Welcome!");
            this.username = username;
            onAuthorization(true);
        } else {
            log1("No such user!");
            this.username = null;
            onAuthorization(false);
        }
    }

    protected boolean saveToFile(String name, String content) {
        try {
            File file = new File("./" + name);
            if(!file.exists()) file.createNewFile();
            FileWriter writer = new FileWriter(file);
            writer.write(content);
            writer.flush();
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    protected String readFromFile(String name) {
        try {
            StringBuilder result = new StringBuilder();
            char nextChar;
            FileReader reader = new FileReader(new File("./" + name));

//            for(int i = 0; i < 11; i++) {
//                nextChar = (char)reader.read();
//                log1("char = " + (int) nextChar);
//                result.append(nextChar);
//            }

            while ((nextChar = (char)reader.read()) != 65535) {
                result.append(nextChar);
            }

            reader.close();
            return result.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
