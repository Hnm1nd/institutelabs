package ru.inf_security.task4;

import ru.inf_security.console.ConsoleApp;

public class Task4 extends ConsoleApp {

    @Override
    protected void onIteration() {
        waitForCommand();
    }

    @Override
    protected void onCommand(String command) {
        switch (command) {
            case "encrypt": {
                crypt("encrypt");
                break;
            }
            case "decrypt": {
                crypt("decrypt");
                break;
            }
            default: {
                if (command.matches("encrypt (.*)")) {
                    encrypt(command.substring(command.indexOf(" ") + 1));
                } else if (command.matches("decrypt (.*)")) {
                    decrypt(command.substring(command.indexOf(" ") + 1));
                } else {
                    log1("No such command");
                }
                break;
            }
        }
    }

    private static String encrypt(String src, String key) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < src.length(); i++) {
            int keyIndex = i;
            if (i != 0) keyIndex %= key.length();
            result.append((char) (src.charAt(i) ^ key.charAt(keyIndex)));
        }
        return result.toString();
    }

    private void crypt(String action) {
        String src;

        log1("Enter the string you want to " + action + ": ");
        src = getScanner().nextLine();

        log0("Enter the keyword: ");

        log1("The result is [" + encrypt(src, getScanner().nextLine()) + "] (Without brackets)");
    }

    private void encrypt(String fileName) {
        String src,
                content;
        log1("Enter the string you want to encrypt: ");
        src = getScanner().nextLine();

        log0("Enter the keyword: ");
        saveToFile(fileName, content = encrypt(src, getScanner().nextLine()));

        log1("The result was saved to file [" + fileName + "]");
        log1("Content of the file: ");
        log1("----------------------");
        log1(content);
        log1("----------------------");
    }

    private void decrypt(String fileName) {
        log0("Enter the keyword: ");
        String key = getScanner().nextLine(),
                content = encrypt(readFromFile(fileName), key);
        log1("Decrypted content:");
        log1("----------------------");
        log1(content);
        log1("----------------------");
    }

}
