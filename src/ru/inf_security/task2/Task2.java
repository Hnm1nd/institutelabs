package ru.inf_security.task2;

import ru.inf_security.console.ConsoleApp;

import java.util.*;

public class Task2 extends ConsoleApp {

    private static final int USERS_COUNT = 6,
            FILES_COUNT = 5,
            ACCESS_LEVELS_COUNT = 5;

    private List<Task2File> fileList;
    private Map<String, Task2User> users;

    @Override
    protected void onInit() {
        Random random = new Random();

        Map<String, Task2User> users = new HashMap<>();
        users.put("admin", new Task2User(0));
        for (int i = 0; i < USERS_COUNT; i++) {
            users.put("user" + i, new Task2User(random.nextInt((ACCESS_LEVELS_COUNT - 1) + 1)));
        }

        setUsers(users);

        List<Task2File> fileList = new ArrayList<>();
        for (int i = 0; i < FILES_COUNT; i++) {
            fileList.add(new Task2File(random.nextInt(ACCESS_LEVELS_COUNT)));
        }
        setFileList(fileList);
    }

    @Override
    protected void onCommand(String command) {
        switch (command.toLowerCase()) {
            case "request": {
                requestFile();
                break;
            }
            case "accesslevel": {
                showAccessLevel();
                break;
            }
            case "list": {
                makeAvailableFileList();
                break;
            }
            default: {
                log1("No such command!");
                break;
            }
        }
    }

    @Override
    protected boolean isUserValid(String username) {
        return users.containsKey(username);
    }

    @Override
    protected void onAuthorization(boolean isSuccessful) {
        if(isSuccessful) {
            showAccessLevel();
            makeAvailableFileList();
        }
    }

    private void makeAvailableFileList() {
        log1("Available objects: ");
        for (int i = 0; i < FILES_COUNT; i++) {
            log1("  Object" + i + ": level " + fileList.get(i).getAccessLevel());
        }
    }

    private void requestFile() {
        log0("Enter the object index: ");
        int index;
        while (true) {
            index = getScanner().nextInt();
            if (index >= FILES_COUNT || index < 0) {
                log1("No such file. Enter the correct index: ");
            } else {
                break;
            }
        }
        int fileAccessLevel = fileList.get(index).getAccessLevel(),
                userAccessLevel = users.get(getUsername()).getAccessLevel();

        if (fileAccessLevel < userAccessLevel) {
            log1("Access denied. Request failed.");
        } else {
            log1("Access granted. Request finished successfully.");
        }
    }

    private void showAccessLevel() {
        log1("Access level: " + users.get(getUsername()).getAccessLevel());
    }

    private void setFileList(List<Task2File> fileList) {
        this.fileList = fileList;
    }

    private void setUsers(Map<String, Task2User> users) {
        this.users = users;
    }

}
