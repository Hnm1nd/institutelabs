package ru.inf_security.task2;

public class Task2User {

    private int accessLevel;

    public Task2User(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public int getAccessLevel() {
        return accessLevel;
    }
}
