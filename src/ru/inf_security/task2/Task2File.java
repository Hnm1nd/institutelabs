package ru.inf_security.task2;

public class Task2File {

    private int accessLevel;

    public Task2File(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public int getAccessLevel() {
        return accessLevel;
    }
}
