package ru.inf_security.task3.data;

public class EncryptedMessage {

    private String key,
            encryptedMessage;

    public EncryptedMessage(String key, String encryptedMessage) {
        this.key = key;
        this.encryptedMessage = encryptedMessage;
    }

    public String getKey() {
        return key;
    }

    public String getEncryptedMessage() {
        return encryptedMessage;
    }
}
