package ru.inf_security.task3;

import ru.inf_security.console.ConsoleApp;
import ru.inf_security.task3.data.EncryptedMessage;

public class Task3 extends ConsoleApp {

    @Override
    protected void onIteration() {
        waitForCommand();
    }

    @Override
    protected void onCommand(String command) {
        switch (command) {
            case "crypt_reshuffle": {
                log1("Enter the string you want to crypt: ");
                cryptLog(getScanner().nextLine());
                break;
            }
            case "decrypt_reshuffle": {
                log1("Enter the string you want to decrypt: ");
                String encryptedMsg = getScanner().nextLine();
                log1("Enter the passkey: ");
                String passkey = getScanner().next();
                decryptLog(encryptedMsg, passkey);
                break;
            }
            case "crypt_replacement": {
                log1("Enter the string you want to crypt: ");
                String msg = getScanner().nextLine();
                log1("Result: \n" + Task3EncryptionModule.cryptByReplacement(msg));
                break;
            }
            case "decrypt_replacement": {
                log1("Enter the string you want to decrypt: ");
                String msg = getScanner().nextLine();
                log1("Result: \n" + Task3EncryptionModule.decryptByReplacement(msg));
                break;
            }
            default: {
                log1("No such command");
                break;
            }
        }
    }

    private void cryptLog(String str) {
        EncryptedMessage message = Task3EncryptionModule.cryptByReshuffle(str);
        log1("Key: " + message.getKey());
        log1("Result (without brackets): \n    [" + message.getEncryptedMessage() + "]");
    }

    private void decryptLog(String str, String passKey) {
        log1("Result: \n    " + Task3EncryptionModule.decryptByReshuffle(str, passKey));
    }

}
