package ru.inf_security.task3;

import ru.inf_security.task3.data.EncryptedMessage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class Task3EncryptionModule {

    static EncryptedMessage cryptByReshuffle(String str) {
        List<List<String>> sourceMatrix = parseSourceStringToMatrix(str);
        List<Integer> columnIndexOrder = new ArrayList<>();
        for (int i = 0; i < sourceMatrix.size() + 1; i++) {
            columnIndexOrder.add(i);
        }
        Collections.shuffle(columnIndexOrder);
        return new EncryptedMessage(makeStringIntegerList(columnIndexOrder),
                makeStringMatrix(swapLines(rotate(sourceMatrix), columnIndexOrder)));
    }

    static String decryptByReshuffle(String str, String passKey) {
        String[] key = passKey.split("");
        List<Integer> columnIndexOrder = new ArrayList<>();
        for (String s : key) {
            columnIndexOrder.add(Integer.valueOf(s));
        }
        return makeStringMatrix(rotate(swapLinesBack(parseEncryptedStringToMatrix(str), columnIndexOrder)));
    }

    static String cryptByReplacement(String src) {
        StringBuilder builder = new StringBuilder();
        String[] msg = src.split("");
        for(String s:msg) {
            builder.append(intValueToString((int) s.charAt(0), 3));
        }
        return builder.toString();
    }

    static String decryptByReplacement(String src) {
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < src.length(); i += 3) {
            int code = Integer.valueOf(src.substring(i, i + 3));
            result.append((char) code);
        }
        return result.toString();
    }

    private static String intValueToString(int value, int symbolCount) {
        StringBuilder result = new StringBuilder(String.valueOf(value));
        while(result.length() != symbolCount) result.insert(0, "0");
        return result.toString();
    }

    private static List<List<String>> parseSourceStringToMatrix(String str) {
        int sizeY = (int) Math.sqrt(str.length()),
                sizeX = sizeY + 1;
        StringBuilder strBuilder = new StringBuilder(str);
        while (strBuilder.length() < sizeX * sizeY) {
            strBuilder.append(" ");
        }
        str = strBuilder.toString();
        return parseStringToCharMatrix(str, sizeY + 1, sizeY);
    }

    private static List<List<String>> parseEncryptedStringToMatrix(String str) {
        int size = (int) Math.sqrt(str.length());
        return parseStringToCharMatrix(str, size, size + 1);
    }

    private static List<List<String>> parseStringToCharMatrix(String str, int sizeX, int sizeY) {
        List<String> source = Arrays.asList(str.split(""));
        List<List<String>> result = new ArrayList<>();
        for (int i = 0; i < sizeY; i++) {
            List<String> row = new ArrayList<>();
            int start = i * sizeX,
                    end = i * sizeX + sizeX;
            if (source.size() < end) {
                end = source.size();
            }
            row.addAll(source.subList(start, end));
            result.add(row);
        }
        return result;
    }

    private static List<List<String>> swapLines(List<List<String>> sourceMatrix, List<Integer> indexOrder) {
        List<List<String>> result = new ArrayList<>();
        for (Integer index : indexOrder) {
            result.add(sourceMatrix.get(index));
        }
        return result;
    }

    private static List<List<String>> swapLinesBack(List<List<String>> sourceMatrix, List<Integer> indexOrder) {
        List<List<String>> result = new ArrayList<>();
        for (int i = 0; i < indexOrder.size(); i++) {
            result.add(sourceMatrix.get(indexOrder.indexOf(i)));
        }
        return result;
    }

    private static List<List<String>> rotate(List<List<String>> sourceMatrix) {
        List<List<String>> result = new ArrayList<>();
        for (int size = 0; size < sourceMatrix.get(0).size(); size++) {
            result.add(new ArrayList<>());
        }
        for (List<String> aSourceMatrix : sourceMatrix) {
            for (int j = 0; j < aSourceMatrix.size(); j++) {
                result.get(j).add(aSourceMatrix.get(j));
            }
        }
        return result;
    }

    private static String makeStringMatrix(List<List<String>> matrix) {
        StringBuilder builder = new StringBuilder();
        for (List<String> list : matrix) builder.append(makeStringList(list));
        return builder.toString();
    }

    private static String makeStringList(List<String> list) {
        StringBuilder builder = new StringBuilder();
        for (String s : list) builder.append(s);
        return builder.toString();
    }

    private static String makeStringIntegerList(List<Integer> intList) {
        StringBuilder builder = new StringBuilder();
        for (Integer i : intList) builder.append(i);
        return builder.toString();
    }

}
