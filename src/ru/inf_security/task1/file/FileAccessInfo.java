package ru.inf_security.task1.file;

import java.util.Random;

public class FileAccessInfo {

    private boolean read = false,
            write = false,
            rightTransfer = false;

    public FileAccessInfo() {
        Random random = new Random();
        read = random.nextBoolean();
        write = random.nextBoolean();
        rightTransfer = random.nextBoolean();
    }

    public FileAccessInfo(String permissions) {
        String perms = permissions.toLowerCase();
        read = perms.contains("r");
        write = perms.contains("w");
        rightTransfer = perms.contains("x");
    }

    public boolean hasPermission(String permission) {
        switch (permission.toLowerCase().charAt(0)) {
            case 'r': {
                return read;
            }
            case 'w': {
                return write;
            }
            case 'x': {
                return rightTransfer;
            }
            default: {
                return false;
            }
        }
    }

    @Override
    public String toString() {
        if (!read && !write && !rightTransfer) return "no rights";
        StringBuilder builder = new StringBuilder();
        if (read) {
            builder.append("r");
        }
        if (write) {
            builder.append("w");
        }
        if (rightTransfer) {
            builder.append("x");
        }
        return builder.toString();
    }
}
