package ru.inf_security.task1.file;

import java.util.HashMap;
import java.util.Map;

public class Task1File {

    private Map<String, FileAccessInfo> userPermissions;

    public Task1File(Map<String, FileAccessInfo> userPermissions) {
        this.userPermissions = userPermissions;
    }

    public Task1File() {
        userPermissions = new HashMap<>();
    }

    public void putPermission(String username, FileAccessInfo permissions) {
        this.userPermissions.put(username, permissions);
    }

    public FileAccessInfo getPermissions(String username) {
        if (userPermissions.containsKey(username)) return userPermissions.get(username);
        return null;
    }

}
