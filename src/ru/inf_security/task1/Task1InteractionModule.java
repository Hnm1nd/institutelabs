package ru.inf_security.task1;

import ru.inf_security.task1.file.FileAccessInfo;
import ru.inf_security.task1.file.Task1File;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Task1InteractionModule {

    private List<Task1File> fileList;

    public Task1InteractionModule() {
        fileList = new ArrayList<>();
    }

    public boolean checkPermission(String username, int objectIndex, String permission) {
        FileAccessInfo info = getPermission(username, objectIndex);
        return info.hasPermission(permission);
    }

    public boolean putPermission(String username, int objectIndex, FileAccessInfo info) {
        Task1File file = getFile(objectIndex);
        if (file == null) return false;
        file.putPermission(username, info);
        return true;
    }

    public Map<Integer, FileAccessInfo> getPermissions(String username) {
        Map<Integer, FileAccessInfo> result = new HashMap<>();
        for (int i = 0; i < fileList.size(); i++) {
            FileAccessInfo accessInfo = fileList.get(i).getPermissions(username);
            if (accessInfo != null) result.put(i, accessInfo);
        }
        return result;
    }

    public FileAccessInfo getPermission(String username, int objectIndex) {
        if (fileList == null || fileList.size() <= objectIndex) return null;
        return fileList.get(objectIndex).getPermissions(username);
    }

    public void makeFiles(int amount) {
        for (int i = 0; i < amount; i++) {
            fileList.add(new Task1File());
        }
    }

    public Task1File getFile(int index) {
        if (fileList == null || fileList.size() <= index || index < 0) return null;
        return fileList.get(index);
    }

}
