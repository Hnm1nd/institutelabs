package ru.inf_security.task1;

import ru.inf_security.console.ConsoleApp;
import ru.inf_security.task1.file.FileAccessInfo;

import java.util.Map;
import java.util.Scanner;

public class Task1 extends ConsoleApp {

    private static final String ADMIN = "admin",
            USER = "user";
    private static final int FILE_COUNT = 6,
            USERS_COUNT = 5;

    private Task1InteractionModule module = new Task1InteractionModule();
    private Scanner scanner = new Scanner(System.in);

    @Override
    protected void onInit() {
        module.makeFiles(FILE_COUNT);
        for (int fileIndex = 0; fileIndex < FILE_COUNT; fileIndex++) {
            module.putPermission(ADMIN, fileIndex, new FileAccessInfo("rwx"));
            for (int userIndex = 0; userIndex < USERS_COUNT; userIndex++) {
                module.putPermission(USER + userIndex, fileIndex, new FileAccessInfo());
            }
        }
    }

    @Override
    protected void onCommand(String command) {
        switch (command.toLowerCase()) {
            case "grant": {
                grantAccess();
                break;
            }
            case "read": {
                read();
                break;
            }
            case "write": {
                write();
                break;
            }
            case "list": {
                makeAvailableFileList();
                break;
            }
            default: {
                log1("No such command!");
                break;
            }
        }
    }

    @Override
    protected boolean isUserValid(String username) {
        return !module.getPermissions(username).isEmpty();
    }

    @Override
    protected void onAuthorization(boolean isSuccessful) {
        if (isSuccessful) {
            makeAvailableFileList();
        }
    }

    private void makeAvailableFileList() {
        log1("Available objects: ");
        Map<Integer, FileAccessInfo> userPermissions = module.getPermissions(getUsername());
        for (Map.Entry<Integer, FileAccessInfo> entry : userPermissions.entrySet()) {
            log1("  Object" + entry.getKey() + ": " + entry.getValue().toString());
        }
    }

    private void read() {
        int fileIndex = chooseFileIndex();
        if (module.checkPermission(getUsername(), fileIndex, "r")) {
            log1("File reading finished!");
        } else {
            log1("You don't have this permission!");
        }
    }

    private void write() {
        int fileIndex = chooseFileIndex();
        if (module.checkPermission(getUsername(), fileIndex, "w")) {
            log1("File writing finished!");
        } else {
            log1("You don't have this permission!");
        }
    }

    private void grantAccess() {
        int fileIndex = chooseFileIndex();
        if (module.checkPermission(getUsername(), fileIndex, "x")) {
            module.putPermission(chooseUsername(), fileIndex, choosePermissions());
            log1("Finished!");
        } else {
            log1("You don't have this permission!");
        }
    }

    private int chooseFileIndex() {
        log0("Select the object: ");
        return Integer.valueOf(scanner.next());
    }

    private String chooseUsername() {
        log0("Enter the username of permissions recipient: ");
        return scanner.next();
    }

    private FileAccessInfo choosePermissions() {
        log0("Enter the permissions(r|w|x): ");
        return new FileAccessInfo(scanner.next());
    }

}
